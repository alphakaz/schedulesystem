class Schedule < ActiveRecord::Base
  attr_accessible :being, :finish, :start, :title, :user_id
  belongs_to :user
end
