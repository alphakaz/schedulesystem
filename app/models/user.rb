class User < ActiveRecord::Base
  attr_accessible :attached, :mail, :name, :turnUp, :password
  has_many :schedules
  validates :name, presence: true, uniqueness: true
  has_secure_password
end
