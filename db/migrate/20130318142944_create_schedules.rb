class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string :title
      t.datetime :start
      t.datetime :finish
      t.boolean :being
      t.integer :user_id

      t.timestamps
    end
  end
end
